﻿using UnityEngine;

namespace fx.addon
{
    public class RagdollRigibody : MonoBehaviour
    {
        public void AddVelocity(Vector3 velocity)
        {
            foreach (Transform child in transform)
            {
                foreach (var rg in child.GetComponentsInChildren<Rigidbody>())
                {
                    rg.AddForce(velocity, ForceMode.Impulse);
                }
            }
        }
    }
}