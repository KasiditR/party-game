﻿using System;
using System.Collections.Generic;
using assets;
using core.entity;
using core.entity.player;
using core.entity.player.gamemode;
using Mirror;
using networking;
using UnityEngine;
using Material = core.Material;

namespace game.admin
{
    public class AdminCommand : NetworkBehaviour
    {
        private struct CommandData
        {
            public Action<string[]> command;
            public string detail;
        }

        private const string CMD_HELP = "help";
        private const string CMD_LOAD_MAP = "m";
        private const string CMD_GAMEMODE = "gm";
        private const string CMD_GIVE_ITEM = "g";
        private const string CMD_DAMAGE = "d";
        private const string CMD_NEXT_GAME = "fs";
        private const string CMD_HAT = "h";

        private readonly Dictionary<string, CommandData> _opCommands = new Dictionary<string, CommandData>();

        private Player _admin;

        public override void OnStartAuthority()
        {
            if (!isServer) return;
            _admin = GetComponent<Player>();
            PlayerCommand.OnPlayerExecuteCommand += OnPlayerExecuteCommandEvent;
            SetupCommands();
        }

        private void OnDisable()
        {
            PlayerCommand.OnPlayerExecuteCommand -= OnPlayerExecuteCommandEvent;
        }

        private void OnPlayerExecuteCommandEvent(Player player, string[] args)
        {
            if (player.GetConnectionId() != _admin.GetConnectionId()) return;
            if (args.Length == 0) return;
            if (!_opCommands.ContainsKey(args[0])) return;
            _opCommands[args[0]].command?.Invoke(args);
        }

        private void SetupCommands()
        {
            _opCommands.Add(CMD_HELP, new CommandData
            {
                command = CmdHelp,
                detail = $"-----[{CMD_HELP}]------",
            });

            _opCommands.Add(CMD_LOAD_MAP, new CommandData
            {
                command = CmdLoadMap,
                detail = $"{CMD_LOAD_MAP}: Load selected map index",
            });

            _opCommands.Add(CMD_GAMEMODE, new CommandData
            {
                command = CmdGameMode,
                detail = $"{CMD_GAMEMODE}: Toggle gamemode",
            });

            _opCommands.Add(CMD_GIVE_ITEM, new CommandData
            {
                command = CmdGiveItem,
                detail = $"{CMD_GIVE_ITEM}: Give item to all players",
            });

            _opCommands.Add(CMD_DAMAGE, new CommandData
            {
                command = CmdDamage,
                detail = $"{CMD_DAMAGE}: damage yourself",
            });

            _opCommands.Add(CMD_NEXT_GAME, new CommandData
            {
                command = CmdNextGame,
                detail = $"{CMD_NEXT_GAME}: Force start next game",
            });

            _opCommands.Add(CMD_HAT, new CommandData
            {
                command = CmdHat,
                detail = $"{CMD_HAT}: Hat",
            });
        }

        private void Info(string message)
        {
            _admin.SendLocalChatMessage(message.Replace("/", string.Empty));
        }

        #region COMMANDS

        private void CmdHelp(string[] args)
        {
            if (args.Length != 1) return;
            foreach (var cmd in _opCommands.Values)
            {
                Info(cmd.detail);
            }
        }

        private void CmdLoadMap(string[] args)
        {
            if (args.Length == 1)
            {
                //Display map list
                for (int i = 0; i < GameAssetInstance.Instance.MapAssetSo.MapCount; i++)
                {
                    MapAssetSO.GameMap gameMap = GameAssetInstance.Instance.MapAssetSo.GetGameMap(i);
                    Info($"{CMD_LOAD_MAP} {i}: {gameMap.mapName}");
                }

                return;
            }

            if (args.Length != 2) return;
            if (!int.TryParse(args[1], out int mapIndex)) return;
            MyNetworkManager.Instance.LoadGameMap(mapIndex, 1);
        }

        private void CmdGameMode(string[] args)
        {
            if (args.Length == 1)
            {
                if (_admin.GetGameMode() == GameMode.SURVIVAL)
                {
                    _admin.SetGameMode(GameMode.SPECTATOR);
                }
                else
                {
                    _admin.SetGameMode(GameMode.SURVIVAL);
                }
            }
        }

        private void CmdGiveItem(string[] args)
        {
            Material[] materials = (Material[])Enum.GetValues(typeof(Material));

            if (args.Length == 1)
            {
                foreach (Material material in materials)
                {
                    Info($"{CMD_GIVE_ITEM} {(int)material}: {material}");
                }
            }

            if (args.Length != 2) return;
            if (!int.TryParse(args[1], out int itemId)) return;
            if (itemId >= materials.Length) return;

            foreach (var player in MyNetworkManager.Instance.GetOnlinePlayers())
            {
                player.SetItemInHand(materials[itemId]);
            }
        }

        private void CmdHat(string[] args)
        {
            Material[] materials = (Material[])Enum.GetValues(typeof(Material));

            if (args.Length == 1)
            {
                foreach (Material material in materials)
                {
                    Info($"{CMD_HAT} {(int)material}: {material}");
                }
            }

            if (args.Length != 2) return;
            if (!int.TryParse(args[1], out int itemId)) return;
            if (itemId >= materials.Length) return;

            foreach (var player in MyNetworkManager.Instance.GetOnlinePlayers())
            {
                player.SetHelmet(materials[itemId]);
            }
        }

        private void CmdDamage(string[] args)
        {
            DeathEffect[] deathEffects = (DeathEffect[])Enum.GetValues(typeof(DeathEffect));
            if (args.Length == 1)
            {
                foreach (var deathEffect in deathEffects)
                {
                    Info($"{CMD_DAMAGE} [damage] {(int)deathEffect} {deathEffect}");
                }
            }

            if (args.Length == 2)
            {
                if (!int.TryParse(args[1], out int damage)) return;
                if (damage < 0) return;
                _admin.Damage(damage, DeathEffect.RAGDOLL);
            }

            if (args.Length == 3)
            {
                if (!int.TryParse(args[1], out int damage)) return;
                if (damage < 0) return;
                if (!int.TryParse(args[2], out int effectIndex)) return;
                if (effectIndex >= deathEffects.Length) return;
                _admin.Damage(damage, deathEffects[effectIndex]);
            }
        }

        private void CmdNextGame(string[] args)
        {
            if (args.Length == 1)
            {
                MyNetworkManager.Instance.LoadRandomMap();
            }
        }

        #endregion

        [ServerCallback]
        private void Update()
        {
            if (!hasAuthority) return;
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                _admin.SendChatMessage($"/{CMD_HELP}");
            }

            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                _admin.SendChatMessage($"/{CMD_NEXT_GAME}");
            }

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                _admin.SendChatMessage($"/{CMD_GAMEMODE}");
            }
        }
    }
}