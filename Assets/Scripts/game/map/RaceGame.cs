﻿using core.entity.player;

namespace game.map
{
    public class RaceGame : BaseGame
    {
        protected override void OnPlayerJoinedEvent(Player player)
        {
            UpdateQualifyBoard();
        }

        protected override void OnQualifyPlayerChanged()
        {
            UpdateQualifyBoard();
        }
    }
}