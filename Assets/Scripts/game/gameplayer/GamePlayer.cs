﻿using System;
using core.entity.player;
using UnityEngine;

namespace game.gameplayer
{
    [Serializable]
    public struct GamePlayer
    {
        public string status;
        public GameObject playerGameObject;

        public Player ToPlayer() => playerGameObject.GetComponent<Player>();
    }
}