﻿using UnityEngine;

namespace game.environment.syncobstacle
{
    public class RotateOverTime : BaseSyncObstacle
    {
        [SerializeField] private float startDelay;
        [SerializeField] private Vector3 axis;
        [SerializeField] private float speed;

        private bool _isStarted;

        protected override void Sync()
        {
            Invoke(nameof(StartDelay), startDelay);
        }

        private void StartDelay()
        {
            _isStarted = true;
        }

        private void FixedUpdate()
        {
            if (!_isStarted) return;
            transform.Rotate(axis * speed);
        }
    }
}