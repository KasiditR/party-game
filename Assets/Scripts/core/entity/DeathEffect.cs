﻿namespace core.entity
{
    public enum DeathEffect
    {
        NONE,
        BLOOD,
        EXPLODE,
        RAGDOLL,
        TRON,
    }
}