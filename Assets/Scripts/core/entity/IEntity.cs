﻿using System.Collections.Generic;
using UnityEngine;

namespace core.entity
{
    public interface IEntity
    {
        Vector3 GetLocation();
        void SetLocation(Vector3 location);
        GameObject GetIdentity();
        List<IEntity> GetNearByEntities(int size, float checkRadius, float angle = 180f);
    }
}