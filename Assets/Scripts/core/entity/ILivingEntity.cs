﻿using UnityEngine;

namespace core.entity
{
    public interface ILivingEntity : IEntity, IDamageable
    {
        Vector3 GetLookDirection();
        float GetHealth();
        void SetHealth(float value);
        float GetMaxHealth();
        void SetMaxHealth(float value);
        bool IsDead();
    }
}