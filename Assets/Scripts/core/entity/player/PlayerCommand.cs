﻿using System;
using Mirror;

namespace core.entity.player
{
    public class PlayerCommand : NetworkBehaviour
    {
        public static event Action<Player, string[]> OnPlayerExecuteCommand;

        public override void OnStartAuthority()
        {
            Player.OnPlayerChat += OnPlayerChatEvent;
        }

        private void OnDisable()
        {
            Player.OnPlayerChat -= OnPlayerChatEvent;
        }

        private void OnPlayerChatEvent(Player player, string message)
        {
            string[] args = message.Split("/");
            if (args.Length != 2) return;
            OnPlayerExecuteCommand?.Invoke(player, args[1].Split(" "));
        }
    }
}