﻿using Cinemachine;
using Mirror;
using ui.player;
using ui.player.scoreboard;
using UnityEngine;

namespace core.entity.player
{
    public class PlayerComponentEnable : NetworkBehaviour
    {
        [Header("CAMERA")] [SerializeField] private CinemachineFreeLook camPrefab;
        [SerializeField] private Camera playerCamera;

        private CinemachineFreeLook _freeLookCamera;
        private PlayerInput playerInput;
        private PlayerInteract playerInteract;
        private PlayerMovement playerMovement;
        private PlayerAnimation playerAnimation;
        private PlayerVoiceChat playerVoiceChat;
        
        private ScoreBoard scoreBoard;
        private PlayerUIManager playerUIManager;
        private Player player;

        private void Awake()
        {
            playerInput = GetComponent<PlayerInput>();
            playerInteract = GetComponent<PlayerInteract>();
            playerMovement = GetComponent<PlayerMovement>();
            playerAnimation = GetComponent<PlayerAnimation>();
            playerVoiceChat = GetComponent<PlayerVoiceChat>();

            scoreBoard = GetComponent<ScoreBoard>();
            playerUIManager = GetComponent<PlayerUIManager>();

            player = GetComponent<Player>();
            
            _freeLookCamera = FindObjectOfType<CinemachineFreeLook>();

            if (!_freeLookCamera)
            {
                _freeLookCamera = Instantiate(camPrefab);
            }
        }

        public override void OnStartLocalPlayer()
        {
            SetCameraFollow(player.transform);
            SetCameraLootAt(player.GetHeadTransform());
            SetPlayerInputEnable(true);
            SetPlayerInteractEnable(true);
            SetPlayerMovementEnable(true);
            SetPlayerVoiceChat(true);
            SetPlayerUIEnable(true);
            SetScoreboardEnable(true);
            playerCamera.gameObject.SetActive(true);
        }

        public void SetVirtualCameraEnable(bool value)
        {
            _freeLookCamera.enabled = value;

            playerCamera.transform.localPosition = Vector3.zero;
            playerCamera.transform.rotation = Quaternion.identity;
        }

        public void SetCameraFollow(Transform followTransform)
        {
            _freeLookCamera.Follow = followTransform;
        }

        public void SetCameraLootAt(Transform lookTransform)
        {
            _freeLookCamera.LookAt = lookTransform;
        }

        public void SetPlayerMovementEnable(bool value)
        {
            playerMovement.enabled = value;
            playerAnimation.enabled = value;
        }

        public void SetPlayerInteractEnable(bool value)
        {
            playerInteract.enabled = value;
        }

        public void SetPlayerVoiceChat(bool value)
        {
            playerVoiceChat.enabled = value;
        }
        public void SetPlayerInputEnable(bool value)
        {
            playerInput.enabled = value;
        }

        public void SetPlayerUIEnable(bool value)
        {
            playerUIManager.enabled = value;
        }

        public void SetScoreboardEnable(bool value)
        {
            scoreBoard.enabled = value;
        }

        public void ResetFollow()
        {
            SetCameraFollow(player.transform);
        }

        public void ResetLookAt()
        {
            SetCameraLootAt(player.GetHeadTransform());
        }
    }
}