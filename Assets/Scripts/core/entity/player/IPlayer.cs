﻿using System.Collections.Generic;
using core.entity.player.gamemode;
using core.interact;
using UnityEngine;

namespace core.entity.player
{
    public interface IPlayer
    {
        void SetWalkSpeed(float value);
        float GetWalkSpeed();
        void SendChatMessage(string value);
        void SendLocalChatMessage(string value);
        void AddVelocity(Vector3 velocity);
        void SetVisible(bool visible);
        bool IsVisible();
        void SetGameMode(GameMode gameMode);
        GameMode GetGameMode();
        void SetFreeze(bool freeze);
        bool IsFreeze();
        void SetInteractable(bool isAllow);
        bool IsInteractAble();
        Transform GetHeadTransform();
        void SetCollider(bool value);
        string GetDisplayName();
        void SetDisplayName(string value);
        List<InteractableObject> GetNearByInteractableObjects(int size, float checkRadius, float angle = 180f);
    }
}