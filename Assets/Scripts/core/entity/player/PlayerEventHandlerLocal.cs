﻿using System;
using assets;
using core.entity.player.gamemode;
using core.interact;
using core.inventory;
using core.inventory.item;
using fx;
using fx.addon;
using scriptsableobject;
using sfx;
using UnityEngine;
using UnityEngine.VFX;
using utils;

namespace core.entity.player
{
    public class PlayerEventHandlerLocal : MonoBehaviour
    {
        //---------------------------------------------
        // ART ONLY
        //---------------------------------------------
        [Header("SFX")] [SerializeField] private EnumValue ragdollSFX;
        [SerializeField] private EnumValue knockSFX;
        [SerializeField] private EnumValue explodeSFX;
        [SerializeField] private EnumValue goreSFX;
        [SerializeField] private EnumValue damageSFX;

        [Header("DEATH FX")] [SerializeField] private EnumValue normalFX;
        [SerializeField] private EnumValue deadFX;
        [SerializeField] private EnumValue ragdollFX;
        [SerializeField] private EnumValue tronFX;
        [SerializeField] private EnumValue damageFX;

        [Header("SETTINGS")] [SerializeField] private float ragdollForce;
        [SerializeField] private float tronFXForce;
        [Header("HAND")] [SerializeField] private Transform handOffset;
        [Header("ARMOR")] [SerializeField] private Transform headOffset;
        [Header("VISIBLE")] [SerializeField] private Transform modelGroupTransform;
        [SerializeField] private Transform nametagTransform;

        private void OnEnable()
        {
            Player player = GetComponent<Player>();
            player.OnArmorChanged += OnArmorChangedEvent;
            player.OnHeldItemSlotChanged += OnHeldItemSlotChangedEvent;
            player.OnSwapHandItem += OnSwapHandItemEvent;
            player.OnHealthChanged += OnHealthChangedEvent;
            player.OnPlayerDeath += OnPlayerDeathEvent;
            player.OnPlayerColliderChanged += OnPlayerColliderChangedEvent;
            player.OnPlayerVisibleChanged += OnPlayerVisibleChangedEvent;
            player.OnPlayerGameModeChanged += OnPlayerGameModeChangedEvent;
            player.OnPlayerInteractableChanged += OnPlayerInteractableChangedEvent;
            player.OnPlayerFreeze += OnPlayerFreezeEvent;
            player.OnPlayerFreeze += OnPlayerFreezeEvent;

            PlayerInteract playerInteract = GetComponent<PlayerInteract>();
            playerInteract.OnPlayerUseItem += OnPlayerUseItemEvent;

            PlayerMovement playerMovement = GetComponent<PlayerMovement>();
            playerMovement.OnKnockout += OnKnockoutEvent;
        }

        private void OnDisable()
        {
            Player player = GetComponent<Player>();
            player.OnArmorChanged -= OnArmorChangedEvent;
            player.OnHeldItemSlotChanged -= OnHeldItemSlotChangedEvent;
            player.OnSwapHandItem -= OnSwapHandItemEvent;
            player.OnHealthChanged -= OnHealthChangedEvent;
            player.OnPlayerDeath -= OnPlayerDeathEvent;
            player.OnPlayerColliderChanged -= OnPlayerColliderChangedEvent;
            player.OnPlayerVisibleChanged -= OnPlayerVisibleChangedEvent;
            player.OnPlayerGameModeChanged -= OnPlayerGameModeChangedEvent;
            player.OnPlayerInteractableChanged -= OnPlayerInteractableChangedEvent;
            player.OnPlayerFreeze -= OnPlayerFreezeEvent;

            PlayerInteract playerInteract = GetComponent<PlayerInteract>();
            playerInteract.OnPlayerUseItem -= OnPlayerUseItemEvent;

            PlayerMovement playerMovement = GetComponent<PlayerMovement>();
            playerMovement.OnKnockout -= OnKnockoutEvent;
        }

        //------------------------------
        // LOCAL PLAYER EVENT
        //------------------------------

        private void OnPlayerVisibleChangedEvent(Player player, bool visible)
        {
            modelGroupTransform.gameObject.SetActive(visible);
            nametagTransform.gameObject.SetActive(visible);
        }

        private void OnPlayerColliderChangedEvent(Player player, bool value)
        {
            Rigidbody rb = GetComponent<PlayerMovement>().rb;
            rb.useGravity = value;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            rb.isKinematic = !value;
            GetComponent<Collider>().enabled = value;
        }

        private void OnPlayerGameModeChangedEvent(Player player, GameMode gameMode)
        {
            if (!player.isLocalPlayer) return;
            PlayerSpectatorMode playerSpectatorMode = GetComponent<PlayerSpectatorMode>();
            playerSpectatorMode.SetSpectator(gameMode == GameMode.SPECTATOR);
            if (gameMode == GameMode.SPECTATOR)
            {
                playerSpectatorMode.FindPlayerToFollow(true);
            }
        }

        private void OnPlayerFreezeEvent(Player player, bool value)
        {
            GetComponent<PlayerMovement>().enabled = !value;
        }

        private void OnPlayerInteractableChangedEvent(Player player, bool value)
        {
            GetComponent<PlayerInteract>().enabled = value;
        }

        private void OnHealthChangedEvent(HumanEntity humanEntity, float oldHealth, float health)
        {
            if (humanEntity is not Player player) return;
            SoundInstance.Instance.PlaySound(damageSFX, player.GetLocation());

            if (player.isLocalPlayer)
            {
                CameraShakeInstance.Instance.Shake(7, 0.5f);
            }

            float damage = oldHealth - health;
            if (damage < 20f) return;
            FXInstance.PlayEffect(damageFX, player.GetLocation(), Quaternion.identity);
            SoundInstance.Instance.PlaySound(goreSFX, player.GetLocation());
        }

        private void OnPlayerDeathEvent(HumanEntity humanEntity, DeathEffect deathEffect)
        {
            if (humanEntity is not Player player) return;
            switch (deathEffect)
            {
                case DeathEffect.BLOOD:
                    FXInstance.PlayEffect(normalFX, player.GetHeadTransform().position, Quaternion.identity);
                    SoundInstance.Instance.PlaySound(goreSFX, player.GetLocation());
                    break;
                case DeathEffect.EXPLODE:
                    FXInstance.PlayEffect(deadFX, player.GetHeadTransform().position, Quaternion.identity);
                    SoundInstance.Instance.PlaySound(explodeSFX, player.GetLocation());
                    break;
                case DeathEffect.RAGDOLL:
                    FXParticle ragdollFx =
                        FXInstance.PlayEffect(ragdollFX, player.GetLocation(), humanEntity.transform.rotation);
                    if (!ragdollFx.TryGetComponent(out RagdollRigibody ragdollRigibody)) return;
                    Vector3 ragdollVelocity =
                        player.GetComponent<PlayerMovement>().rb.velocity.normalized * ragdollForce;
                    ragdollRigibody.AddVelocity(ragdollVelocity);
                    SoundInstance.Instance.PlaySound(ragdollSFX, player.GetLocation());
                    break;
                case DeathEffect.TRON:
                    FXParticle tronFx =
                        FXInstance.PlayEffect(tronFX, player.GetLocation(), humanEntity.transform.rotation);
                    foreach (Transform child in tronFx.transform)
                    {
                        if (!child.TryGetComponent(out VisualEffect vfx)) continue;
                        vfx.SetVector3("Velocity",
                            player.GetComponent<PlayerMovement>().rb.velocity.normalized * tronFXForce);
                    }

                    break;
            }
        }

        private void OnKnockoutEvent()
        {
            SoundInstance.Instance.PlaySound(knockSFX);
        }

        private void OnHeldItemSlotChangedEvent(HumanEntity humanEntity, int oldSlot, int slot)
        {
            OnSwapHandItemEvent(humanEntity, humanEntity.GetItem(slot));
        }

        private void OnArmorChangedEvent(HumanEntity humanEntity, ItemStack itemStack, ArmorType armorType)
        {
            switch (armorType)
            {
                case ArmorType.HELMET:
                    SpawnWorldItem(itemStack, headOffset);
                    break;
                case ArmorType.CHESTPLATE:
                    break;
                case ArmorType.LEGGINGS:
                    break;
                case ArmorType.BOOTS:
                    break;
            }
        }

        private WorldItem SpawnWorldItem(ItemStack itemStack, Transform container)
        {
            WorldItem worldItem = null;
            string identity = $"[{itemStack.material}]{itemStack.itemName}";
            foreach (Transform child in container)
            {
                bool isSameItem = child.name.Equals(identity);
                if (isSameItem)
                {
                    worldItem = child.GetComponent<WorldItem>();
                }

                child.gameObject.SetActive(isSameItem);
            }

            if (worldItem) return worldItem;

            ItemAssetSO.ItemAsset itemAsset = GameAssetInstance.Instance.ItemAssets.GetItemAsset(itemStack.material);
            worldItem = Instantiate(itemAsset.worldItem, container);
            worldItem.name = identity;
            worldItem.transform.localPosition = Vector3.zero;
            return worldItem;
        }

        private void OnSwapHandItemEvent(HumanEntity humanEntity, ItemStack itemStack)
        {
            handItem = SpawnWorldItem(itemStack, handOffset);
        }

        private WorldItem handItem;

        private void OnPlayerUseItemEvent(Player player, ClickActionType clickActionType)
        {
            if (!handItem)
            {
                handItem = SpawnWorldItem(player.GetItemInHand(), handOffset);
            }

            handItem.Init(player, clickActionType);
            handItem.Interact();
        }
    }
}