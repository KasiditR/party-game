﻿namespace ui.voicechat
{
    public interface IVoiceChatUI : IVisible
    {
        void SetVolume(float volume);
    }
}