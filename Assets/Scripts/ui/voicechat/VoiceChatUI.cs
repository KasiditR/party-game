﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.voicechat
{
    public class VoiceChatUI : MonoBehaviour, IVoiceChatUI
    {
        [SerializeField] private Image volumeImage;

        private void Awake()
        {
            Visible = false;
            volumeImage.fillAmount = 0;
        }

        public void SetVolume(float volume)
        {
            volumeImage.fillAmount = volume;
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}