﻿using UnityEngine;
using UnityEngine.UI;

namespace ui.mainmenu
{
    public class MainMenuUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button hostGameButton;
        [SerializeField] private Button findGameButton;

        private void Awake()
        {
            Debug.Assert(hostGameButton != null, $"{nameof(hostGameButton)}== null");
            Debug.Assert(findGameButton != null, $"{nameof(findGameButton)}== null");

            hostGameButton.onClick.AddListener(() =>
            {
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.HOSTGAME);
            });
            findGameButton.onClick.AddListener(() =>
            {
                MainMenuManager.Instance.OpenMenu(MainMenuManager.EMenu.SERVERLIST);
            });
        }

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}