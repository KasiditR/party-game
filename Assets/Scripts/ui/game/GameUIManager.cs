﻿using System;
using Mirror;
using UnityEngine;

namespace ui.game
{
    public class GameUIManager : NetworkBehaviour
    {
        [Serializable]
        public struct PlayerScoreData
        {
            public string header;
            public int score;
            public Color color;
        }

        [Serializable]
        public struct GameInfoData
        {
            public string header;
            public string info;
        }

        [Serializable]
        public struct TeamPlayerData
        {
            public int good;
            public int bad;
        }
        
        [Space] [SyncVar(hook = nameof(OnGameTimerChanged))]
        public int timer;

        [SyncVar(hook = nameof(OnPlayerAliveChanged))]
        public int alive;

        [SyncVar(hook = nameof(OnPlayerLeftChanged))]
        public int playerLeft;

        [SyncVar(hook = nameof(OnTeamAliveChanged))]
        public TeamPlayerData team;

        [SyncVar(hook = nameof(OnGameInfoChanged))]
        public GameInfoData info;

        private IGameAliveUI _aliveUI;
        private IGameTeamAliveUI _teamAliveUI;
        private IGameTimerUI _timeUI;
        private IGameScoreUI _scoreUI;
        private IGameInfoUI _infoUI;
        private IGamePlayerLeftUI _gamePlayerLeft;

        private void Awake()
        {
            _aliveUI = GetComponentInChildren<IGameAliveUI>();
            _aliveUI.Visible = false;

            _timeUI = GetComponentInChildren<IGameTimerUI>();
            _timeUI.Visible = false;

            _scoreUI = GetComponentInChildren<IGameScoreUI>();
            _scoreUI.Visible = false;

            _teamAliveUI = GetComponentInChildren<IGameTeamAliveUI>();
            _teamAliveUI.Visible = false;

            _infoUI = GetComponentInChildren<IGameInfoUI>();
            _infoUI.Visible = false;

            _gamePlayerLeft = GetComponentInChildren<IGamePlayerLeftUI>();
            _gamePlayerLeft.Visible = false;
        }

        private void OnGameTimerChanged(int old, int currentTimer)
        {
            _timeUI.SetTimer(currentTimer);
            _timeUI.Visible = true;
        }

        private void OnPlayerAliveChanged(int old, int currentAlive)
        {
            _aliveUI.SetAlive(currentAlive);
            _aliveUI.Visible = true;
        }

        private void OnPlayerLeftChanged(int old, int currentPlayer)
        {
            _gamePlayerLeft.SetPlayerLeft(currentPlayer);
            _gamePlayerLeft.Visible = true;
        }

        private void OnTeamAliveChanged(TeamPlayerData old, TeamPlayerData teamPlayerData)
        {
            _teamAliveUI.SetTeam(teamPlayerData);
            _teamAliveUI.Visible = true;
        }

        private void OnGameInfoChanged(GameInfoData old, GameInfoData infoData)
        {
            _infoUI.SetHeader(infoData.header);
            _infoUI.SetInfo(infoData.info);
            _infoUI.Visible = true;
        }

        public void AddPlayerScore(GameObject target, PlayerScoreData playerScoreData)
        {
            NetworkIdentity identity = target.GetComponent<NetworkIdentity>();
            TargetAddScore(identity.connectionToClient, playerScoreData);
        }

        [TargetRpc]
        private void TargetAddScore(NetworkConnection conn, PlayerScoreData playerScoreData)
        {
            _scoreUI.SetScore(playerScoreData);
            _scoreUI.Visible = true;
        }
    }
}