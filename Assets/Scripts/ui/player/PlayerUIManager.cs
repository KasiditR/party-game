﻿using core.entity;
using core.entity.player;
using core.inventory.item;
using ui.player.chat;
using ui.player.inventory;
using ui.voicechat;
using UnityEngine;

namespace ui.player
{
    public class PlayerUIManager : MonoBehaviour
    {
        [SerializeField] private Transform groupTransform;
        [Header("UI")] [SerializeField] private HealthUI healthUI;
        [SerializeField] private ChatUI chatUI;
        [SerializeField] private InventoryUI inventoryUI;
        [SerializeField] private VoiceChatUI voiceChatUI;
        [SerializeField] private PauseMenuUI pauseMenuUI;

        private void OnEnable()
        {
            Player.OnPlayerChat += OnPlayerChatEvent;

            Player player = GetComponent<Player>();
            player.OnHealthChanged += OnHealthChangedEvent;
            player.OnSwapHandItem += OnSwapHandItemEvent;
            player.OnHeldItemSlotChanged += OnHeldItemSlotChangedEvent;
            groupTransform.gameObject.SetActive(true);

            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction += OnInputActionEvent;

            PlayerVoiceChat playerVoiceChat = GetComponent<PlayerVoiceChat>();
            playerVoiceChat.OnPushToTalk += OnPushToTalkEvent;
            playerVoiceChat.OnVoiceChatTypeChanged += OnVoiceChatTypeChangedEvent;

            PlayerVoiceChatHandler playerVoiceChatHandler = GetComponent<PlayerVoiceChatHandler>();
            playerVoiceChatHandler.OnVoiceLevelChanged += OnVoiceLevelChangedEvent;

            voiceChatUI.Visible = false;
            pauseMenuUI.Visible = false;
        }

        private void OnDisable()
        {
            Player.OnPlayerChat -= OnPlayerChatEvent;

            Player player = GetComponent<Player>();
            player.OnHealthChanged -= OnHealthChangedEvent;
            player.OnSwapHandItem -= OnSwapHandItemEvent;
            player.OnHeldItemSlotChanged -= OnHeldItemSlotChangedEvent;
            groupTransform.gameObject.SetActive(false);

            PlayerInput playerInput = GetComponent<PlayerInput>();
            playerInput.OnInputAction -= OnInputActionEvent;

            PlayerVoiceChat playerVoiceChat = GetComponent<PlayerVoiceChat>();
            playerVoiceChat.OnPushToTalk -= OnPushToTalkEvent;
            playerVoiceChat.OnVoiceChatTypeChanged -= OnVoiceChatTypeChangedEvent;

            PlayerVoiceChatHandler playerVoiceChatHandler = GetComponent<PlayerVoiceChatHandler>();
            playerVoiceChatHandler.OnVoiceLevelChanged -= OnVoiceLevelChangedEvent;
        }

        private void Start()
        {
            Player player = GetComponent<Player>();
            ForceUpdateInventory(player);
        }

        private void OnHealthChangedEvent(HumanEntity humanEntity, float oldHealth, float health)
        {
            healthUI.SetHealth(health, humanEntity.GetMaxHealth());
        }

        private void OnSwapHandItemEvent(HumanEntity humanEntity, ItemStack itemStack)
        {
            Player player = (Player)humanEntity;
            ForceUpdateInventory(player);
        }

        private void OnHeldItemSlotChangedEvent(HumanEntity humanEntity, int oldSlot, int slot)
        {
            inventoryUI.SetHeldSlot(slot);

            Player player = (Player)humanEntity;
            ForceUpdateInventory(player);
        }

        private void ForceUpdateInventory(Player player)
        {
            for (int i = 0; i < player.GetInventorySize(); i++)
            {
                inventoryUI.SetItemSlot(player.GetItem(i), i);
            }
        }

        #region VOICE CHAT

        private PlayerVoiceChat.VoiceChatType _voiceChatType = PlayerVoiceChat.VoiceChatType.PUSH_TO_TALK;

        private void OnVoiceChatTypeChangedEvent(PlayerVoiceChat.VoiceChatType voiceChatType)
        {
            _voiceChatType = voiceChatType;
        }

        private void OnPushToTalkEvent(bool active)
        {
            voiceChatUI.SetVolume(0);
            voiceChatUI.Visible = active;
        }

        private void OnVoiceLevelChangedEvent(float volume)
        {
            voiceChatUI.SetVolume(volume);
            if (_voiceChatType == PlayerVoiceChat.VoiceChatType.ACTIVATION)
            {
                voiceChatUI.Visible = volume > 0f;
            }
        }

        #endregion

        private void OnInputActionEvent(PlayerInput.EInputAction inputAction)
        {
            switch (inputAction)
            {
                case PlayerInput.EInputAction.ENTER_KEY_DOWN:
                    chatUI.OnSubmit(GetComponent<Player>());
                    break;
                case PlayerInput.EInputAction.ESC_KEY_DOWN:
                    pauseMenuUI.ToggleMenu();
                    break;
            }
        }

        #region CHAT

        private void OnPlayerChatEvent(Player player, string message)
        {
            chatUI.ChatReceived(message);
            chatUI.Visible = true;
        }

        #endregion
    }
}