﻿using System;
using core.entity.player;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace ui.player.chat
{
    public class ChatUI : MonoBehaviour, IVisible
    {
        [Header("CHAT BOX")] [SerializeField] private int limitHistory = 3;
        [SerializeField] private float chatStayDuration;
        [SerializeField] private float fadeLength;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Transform chatBoxContainer;
        [SerializeField] private ChatMessageItem chatMessageItemTemplate;
        [Header("SEND CHAT")] [SerializeField] private TMP_InputField chatInput;

        private void Awake()
        {
            chatMessageItemTemplate.gameObject.SetActive(false);
            Visible = false;
        }

        #region ChatBox

        public void ChatReceived(string message)
        {
            ChatMessageItem chatMessageItem = Instantiate(chatMessageItemTemplate, chatBoxContainer);
            chatMessageItem.gameObject.SetActive(true);
            chatMessageItem.SetMessage(message);
            CheckHistory();
            
            CancelHide();
            AutoHide();
        }

        private void CancelHide()
        {
            canvasGroup.alpha = 1;
            DOTween.Kill(canvasGroup);
        }

        private void AutoHide()
        {
            canvasGroup.DOFade(1, chatStayDuration).OnComplete(() =>
            {
                canvasGroup.DOFade(0, fadeLength)
                    .OnComplete(() =>
                    {
                        canvasGroup.alpha = 1;
                        Visible = false;
                    });
            });
        }

        private void CheckHistory()
        {
            if (chatBoxContainer.childCount < limitHistory + 1) return;
            Destroy(chatBoxContainer.GetChild(1).gameObject);
        }

        #endregion

        #region SEND CHAT

        public void OnSubmit(Player player)
        {
            CancelHide();
            
            if (Visible)
            {
                if (HasMessage())
                {
                    string chatFormat = $"{player.GetDisplayName()}: {chatInput.text}";
                    player.SendChatMessage(chatFormat);
                    chatInput.text = string.Empty;
                    chatInput.readOnly = true;
                }
                else
                {
                    if (chatInput.readOnly)
                    {
                        chatInput.readOnly = false;
                        chatInput.Select();
                        chatInput.ActivateInputField();
                    }
                    else
                    {
                        chatInput.readOnly = true;
                        AutoHide();
                    }
                }
            }
            else
            {
                Visible = true;
                
                chatInput.readOnly = false;
                chatInput.Select();
                chatInput.ActivateInputField();
            }
        }

        private bool HasMessage()
        {
            return !string.IsNullOrEmpty(chatInput.text);
        }

        #endregion

        public bool Visible
        {
            get => gameObject.activeSelf;
            set => gameObject.SetActive(value);
        }
    }
}