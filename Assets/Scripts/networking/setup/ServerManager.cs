﻿using game.environment.syncobstacle;
using Mirror;
using UnityEngine;

namespace networking.setup
{
    public class ServerManager : NetworkBehaviour
    {
        [Header("SYNC OBSTACLE")] [SerializeField]
        private int syncObstacleDelay = 5;

        [ServerCallback]
        private void Start()
        {
            Invoke(nameof(SyncObstacles), syncObstacleDelay);
        }

        #region SYNC OBSTACLE

        private void SyncObstacles()
        {
            RpcSyncObstacles();
        }

        [ClientRpc]
        private void RpcSyncObstacles()
        {
            foreach (var syncObstacle in FindObjectsOfType<BaseSyncObstacle>())
            {
                syncObstacle.StartSync();
            }
        }

        #endregion
    }
}